/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.beenum;

/**
 * The Type Of Business Entity Determination Trigger Time Point
 *
 * @ClassName: BETriggerTimePointType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum BETriggerTimePointType {
    /**
     * 不执行,用于判断时机比较结果
     */
    None(0),

    /**
     * 数据加载后, 目前来说是retrieve和query时触发
     * <p>
     * 此事件只针对 BODeterminationType.Transient类型的Determination 也就是说不能够操作持久化成员
     * 用于计算虚拟字段值,动作联动等操作
     */
    AfterLoading(1),

    /**
     * 数据更新后
     * 可以操作持久化或非持久化属性和Node
     */
    AfterModify(2),

    /**
     * 校验执行时
     * ValidateAndDetermine 方法中执行
     */
    Determine(4),

    /**
     * 一致性检查前，保存中进行Validation之前进行，是修改数据的最后时机
     */
    BeforeCheck(8),

    /**
     * 生成编码后
     * 用于同步更新根据编号生成规则产生的编号
     */
    AfterNumbersAdjusted(16),

    /**
     * 保存失败后，现阶段可以不加
     * Finalize,Validation执行失败后执行,用于清理缓存垃圾数据等操作
     */
    AfterFailedSaveAttempt(32),

    /**
     * 调用RetrieveDefault时触发
     */
    RetrieveDefault(64),

    /**
     * 调用Query前触发, 仅主节点有效.
     */
    BeforeQuery(128),

    /**
     * 调用Query后触发, 仅主节点有效.
     */
    AfterQuery(256),

    /**
     * 取消
     */
    Cancel(512),

    /**
     * 检索前，调用retrieve之前触发，仅主节点有效
     */
    BeforeRetrieve(1024),

    /**
     * 保存后(事务内)，调用保存后触发
     */
    AfterSave(2048);

    private static java.util.HashMap<Integer, BETriggerTimePointType> mappings;
    private int intValue;

    private BETriggerTimePointType(int value) {
        intValue = value;
        BETriggerTimePointType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, BETriggerTimePointType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, BETriggerTimePointType>();
        }
        return mappings;
    }

    public static BETriggerTimePointType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}