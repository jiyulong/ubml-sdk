/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import org.openatom.ubml.model.be.definition.operation.BizOperationCollection;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

/**
 * The  Josn Serializer Of Biz Operation Collection
 *
 * @ClassName: BizOperationCollectionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class BizOperationCollectionSerializer<T extends BizOperationCollection> extends JsonSerializer<T> {
    @Override
    public void serialize(BizOperationCollection value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        writeBaseProperty(value, gen);

    }

    private void writeBaseProperty(BizOperationCollection operations, JsonGenerator writer) {
        if (operations.size() == 0) {
            SerializerUtils.WriteStartArray(writer);
            SerializerUtils.WriteEndArray(writer);
            return;
        }
        SerializerUtils.WriteStartArray(writer);
        for (int i = 0; i < operations.size(); i++) {
            getConvertor().serialize(operations.get(i), writer, null);
        }
        SerializerUtils.WriteEndArray(writer);
    }

    protected abstract BizOperationSerializer getConvertor();
}
