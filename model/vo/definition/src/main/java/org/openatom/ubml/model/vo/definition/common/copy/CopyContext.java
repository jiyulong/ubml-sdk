/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.common.copy;

import org.openatom.ubml.model.vo.definition.GspViewModel;

/**
 * The Context While Copying
 *
 * @ClassName: CopyContext
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CopyContext {
    /**
     * 源Vo
     */
    private GspViewModel privateOriginVo;

    public final GspViewModel getOriginVo() {
        return privateOriginVo;
    }

    public final void setOriginVo(GspViewModel value) {

        privateOriginVo = value;
    }

    /**
     * 新Vo的元数据头节点
     */
    private CopyContext privateMetadataHeader;

    public final CopyContext getMetadataHeader() {

        return privateMetadataHeader;
    }

    public final void setMetadataHeader(CopyContext value) {
        privateMetadataHeader = value;
    }

    private boolean includeVoActions = false;

    /**
     * 是否拷贝Vo操作
     */
    boolean getIncludeVoActions() {
        return this.includeVoActions;
    }

    public final void setIncludeVoActions(boolean value) {
        if (value) {
            throw new RuntimeException("vo元数据拷贝暂不支持拷贝操作。");
        }
        includeVoActions = value;
    }
}
