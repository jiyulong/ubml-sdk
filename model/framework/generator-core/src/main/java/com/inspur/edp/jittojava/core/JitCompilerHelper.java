/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.core;

import com.inspur.edp.jittojava.context.common.JitCompilerConfigLoader;
import com.inspur.edp.jittojava.context.entity.JitCompilerConfigration;
import com.inspur.edp.jittojava.spi.AfterGeneratorAction;
import com.inspur.edp.jittojava.spi.JitAction;
import java.util.List;

/**
 * @Classname JitCompilerHelper
 * @Description TODO
 * @Date 2019/7/29 16:06
 * @Created by zhongchq
 * @Version 1.0
 */
public class JitCompilerHelper extends JitCompilerConfigLoader {

    private static JitCompilerHelper singleton = null;

    public JitCompilerHelper() {
    }

    public static JitCompilerHelper getInstance() {
        if (singleton == null) {
            singleton = new JitCompilerHelper();
        }
        return singleton;
    }

    /**
     * @param typeName
     * @return com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer
     * @throws
     * @author zhongchq
     * @description 返回各元数据序列化器
     * @date 9:34 2019/7/24
     **/
    public JitAction getManager(String typeName) {
        JitAction manager = null;
        JitCompilerConfigration data = getCompilerConfigurations(typeName);
        if (data != null && data.getEnable()) {
            Class<?> cls = null;
            if (data.getBeforeCompiler() != null) {
                try {
                    cls = Class.forName(data.getBeforeCompiler().getName());
                    manager = (JitAction) cls.newInstance();
                } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return manager;
    }

    public AfterGeneratorAction getAfterActionManager(String typeName) {
        AfterGeneratorAction manager = null;
        JitCompilerConfigration data = getCompilerConfigurations(typeName);
        if (data != null && data.getEnable()) {
            Class<?> cls = null;
            if (data.getAfterCompiler() != null) {
                try {
                    cls = Class.forName(data.getAfterCompiler().getName());
                    manager = (AfterGeneratorAction) cls.newInstance();
                } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return manager;
    }

    public List<JitCompilerConfigration> getCompilerTypeList() {
        return getCompilerConfigurations();
    }
}
