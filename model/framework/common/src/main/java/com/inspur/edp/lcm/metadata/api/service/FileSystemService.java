/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.service;

import com.inspur.edp.lcm.metadata.api.entity.FileType;
import java.io.IOException;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;

/**
 * @author zhaoleitr
 */
public interface FileSystemService {
    /**
     * 展示到浏览器
     *
     * @param path 路径
     * @throws IOException
     */
    void showInExplorer(String path) throws IOException;

    FileAltMonitor createFileMonitor(String path, FileType type, FileAlterationListenerAdaptor adaptor);
}
