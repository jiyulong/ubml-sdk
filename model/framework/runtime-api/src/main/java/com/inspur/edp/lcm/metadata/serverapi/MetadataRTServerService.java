/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.serverapi;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.Metadata4Ref;
import com.inspur.edp.lcm.metadata.api.entity.MetadataCustomizationFilter;
import com.inspur.edp.lcm.metadata.api.entity.MetadataDto;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackage;
import com.inspur.edp.lcm.metadata.api.entity.MetadataRTFilter;
import com.inspur.edp.lcm.metadata.api.entity.ServiceUnitInfo;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;
import java.util.List;
import java.util.Map;

@GspServiceBundle(applicationName = "runtime", serviceUnitName = "Lcm", serviceName = "metadata-rt")
public interface MetadataRTServerService {

    /**
     * 根据元数据id获取运行时元数据
     *
     * @param id 元数据id
     * @return 元数据内容实体
     */
    MetadataDto getMetadataDtoRTByID(@RpcParam(paramName = "id") String id);

    /**
     * 根据元数据ID获取元数据
     *
     * @param id 元数据id
     * @return 元数据内容实体
     */
    MetadataDto getMetadataDto(@RpcParam(paramName = "id") String id);

    /**
     * 根据元数据id获取元数据
     *
     * @param id 元数据ID
     * @return 元数据内容实体
     */
    GspMetadata getMetadata(String id);

    /**
     * @Author Robin
     * @Description 加载服务端所有的元数据
     * @Date 10:02 2019/8/20
     * @Param []
     **/
    void loadAllMetadata();

    /**
     * 根据元数据类型获取元数据
     *
     * @param metadataTypes 元数据类型类别，以逗号拼接
     * @return 序列化的元数据信息列表
     */
    String getMetadataRefDtoListWithTypes(String metadataTypes);

    /**
     * @return 元数据信息列表
     */
    String getMetadataRefDtoList();

    /**
     * 获取所有元数据信息列表
     *
     * @return 元数据信息列表
     */
    List<Metadata4Ref> getMetadataRefList();

    /**
     * 根据类型获取元数据信息列表
     *
     * @param metadataTypes
     * @return
     */
    List<Metadata4Ref> getMetadataRefListWithTypes(@RpcParam(paramName = "metadataTypes") String metadataTypes);

    /**
     * 根据过滤条件获取元数据信息列表
     *
     * @param filter 过滤条件
     * @return 元数据信息列表
     */
    String getMetadataDtoListByFilter(@RpcParam(paramName = "filter") String filter);

    /**
     * 根据逻辑ID获取元数据
     *
     * @param metadataNamespace 命名空间
     * @param metadataCode      元数据编号
     * @param metadataTypeCode  元数据类型
     * @return 元数据实体
     */
    MetadataDto getMetadataDtoBySemanticID(@RpcParam(paramName = "metadataNamespace") String metadataNamespace,
        @RpcParam(paramName = "metadataCode") String metadataCode,
        @RpcParam(paramName = "metadataTypeCode") String metadataTypeCode);

    /**
     * 根据过滤条件虎丘元数据信息列表
     *
     * @param filter 过滤条件
     * @return 元数据信息列表
     */
    List<Metadata4Ref> getMetadataListByFilter(MetadataRTFilter filter);

    /**
     * 根据元数据ID获取SU信息
     *
     * @param metadataId 元数据ID
     * @return 元数据所属SU信息
     */
    ServiceUnitInfo getServiceUnitInfo(@RpcParam(paramName = "metadataId") String metadataId);

    /**
     * 获取元数据
     *
     * @param metadataCustomizationFilter 运行时定制元数据过滤器
     * @return 元数据内容实体
     */
    String getMetadataString(
        @RpcParam(paramName = "metadataCustomizationFilter") MetadataCustomizationFilter metadataCustomizationFilter);

    /**
     * 根据路径递归获取路径下元数据包中元数据列表
     *
     * @param path 路径
     * @return 元数据列表
     */
    List<GspMetadata> getMetadataListRecursivly(String path);

    /**
     * 获取路径下元数据包列表
     *
     * @param path 路径
     * @return 元数据包列表
     */
    Map<String, MetadataPackage> getMetadataPackagesRecursivly(String path);

    /**
     * 根据元数据包路径及元数据包信息获取元数据列表
     *
     * @param packagePath 元数据包路径
     * @return 元数据列表
     */
    List<GspMetadata> getMetadataListFromPackage(String packagePath);
}
