/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.jittojava.context.common;

//import org.junit.Assert;
//import org.junit.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//import static org.hamcrest.Matchers.is;

//@SpringBootTest
public class FileUtilsTest {
    private static String TEST_FILE_NAME = "/fileUtils-test-file.txt";
    private static String EXPECT_RESULT = "junit test";

//    @Test
//    public void fileReadTest() {
//        String testFilePath = this.getClass().getResource(TEST_FILE_NAME).getPath();
//        String content = FileUtils.fileRead(testFilePath);
//        Assert.assertThat(content, is(EXPECT_RESULT));
//    }
}