/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.context.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @Classname FileUtils
 * @Description TODO
 * @Date 2019/7/29 16:26
 * @Created by zhongchq
 * @Version 1.0
 */
public class FileUtils {

    /**
     * @param path
     * @return java.lang.String
     * @throws
     * @author zhongchq
     * @description 读取文件内容，返回文件内容的字符串。
     * @date 13:54 2019/7/19
     **/
    public static String fileRead(String path) {
        String encoding = "UTF-8";
        File file = new File(path);
        Long filelength = file.length();
        byte[] filecontent = new byte[filelength.intValue()];
        try {
            FileInputStream in = new FileInputStream(file);
            in.read(filecontent);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            //判断有没有utf-8 bom头。有则去除。
            String fileContents = new String(filecontent, encoding);
            if (fileContents.startsWith("\ufeff")) {

                fileContents = fileContents.substring(1);

            }
            return fileContents;
        } catch (UnsupportedEncodingException e) {
            System.err.println("The OS does not support " + encoding);
            e.printStackTrace();
            return null;
        }
    }

}
